﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Play_Game_Menu : MonoBehaviour {
    public string namaScene;
    public void Load_Play_Game_Menu() 
    {
        SceneManager.LoadScene(namaScene);
    }
}
