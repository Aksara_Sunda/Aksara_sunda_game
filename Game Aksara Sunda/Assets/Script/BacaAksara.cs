﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BacaAksara : MonoBehaviour {

    public Image Soal;
    public float Timers = 10;
    public Text Waktu,Score;
    public InputField Jawaban;
    public bool Sudah = false;
    public string JawabanBenar;

    public GameObject Win, Lose;

    private void Start()
    {
        Score.text = "Score : " + PlayerPrefs.GetInt("Score").ToString();
    }

    private void Update()
    {
        if (Timers>=0 && Sudah ==false)
        {
            Timers -= 1 * Time.deltaTime;
        }else
        {
            Lose.SetActive(false);
        }

        Waktu.text = Timers.ToString("0");
    }

    public void Jawab()
    {
        if (Jawaban.text =="")
        {
            print("Kosong");
        }
        else if (Jawaban.text == JawabanBenar)
        {
            PlayerPrefs.SetInt("Score", PlayerPrefs.GetInt("Score") + 10);
            Score.text = "Score : " + PlayerPrefs.GetInt("Score").ToString();
            Sudah = true;
            Win.SetActive(true);
        }else
        {
            Lose.SetActive(true);
        }
    }

    public void Reset()
    {
        Application.LoadLevel("Baca_Aksara_Level");
    }
}
