﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class About : MonoBehaviour {
    public void Load_About() 
    {
        SceneManager.LoadScene("About");
    }
}
