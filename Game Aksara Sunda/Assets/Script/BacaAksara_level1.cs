﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BacaAksara_level1 : MonoBehaviour {

    public void Level(int Pil)
    {
        switch(Pil)
        {
            case 1:
                if (PlayerPrefs.GetInt("Score") >= 0)
                {
                    PlayerPrefs.SetInt("Score", 0);
                    SceneManager.LoadScene("Baca_Aksara_L1");
                }
                break;
            case 2:
                if (PlayerPrefs.GetInt("Score") >= 10)
                {
                    SceneManager.LoadScene("Baca_Aksara_L2");
                }
                break;
            case 3:
                if (PlayerPrefs.GetInt("Score") >= 20)
                {
                    SceneManager.LoadScene("Baca_Aksara_L3");
                }
                break;
            case 4:
                if (PlayerPrefs.GetInt("Score") >= 30)
                {
                    SceneManager.LoadScene("Baca_Aksara_L4");
                }
                break;
            case 5:
                if (PlayerPrefs.GetInt("Score") >= 40)
                {
                    SceneManager.LoadScene("Baca_Aksara_L5");
                }
                break;
        }
    }
}
